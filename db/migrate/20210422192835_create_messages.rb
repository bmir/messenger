class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages do |t|
      t.string :content
      t.string :destination
      t.string :context_id, index: true

      t.timestamps
    end
  end
end
