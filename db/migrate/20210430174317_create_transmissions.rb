class CreateTransmissions < ActiveRecord::Migration[6.1]
  def change
    create_table :transmissions do |t|
      t.string :status
      t.string :transmission_context_id
      t.string :message_context_id

      t.timestamps
    end
  end
end
