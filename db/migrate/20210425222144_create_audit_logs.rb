class CreateAuditLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :audit_logs do |t|
      t.string :context_id
      t.string :content

      t.timestamps
    end
  end
end
