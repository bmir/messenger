class Transmission < ApplicationRecord
  validates :transmission_context_id, presence: true
  validates :message_context_id, presence: true
  validates :status, presence: true
end
