class Message < ApplicationRecord
  include Utils::String::Random
  before_create :generate_context_id

  validates :content, presence: true
  validates :destination, presence: true

  has_one :transmission, foreign_key: :message_context_id, primary_key: :context_id

  def generate_context_id
    self.context_id = hashed_random_string
  end
end
