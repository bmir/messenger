class AuditLog < ApplicationRecord
  validates :content, presence: true
  validates :context_id, presence: true
end
