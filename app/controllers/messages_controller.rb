class MessagesController < ApplicationController
  include Utils::Json::Response
  include Constants::Http

  DEFAULT_LIST_SIZE = 50

  def index
    offset_id = params[:offset_id]
    size = params[:size] || DEFAULT_LIST_SIZE
    search_destination = params[:search_destination]

    messages = Message.all.limit(size).order(updated_at: :desc)
    messages = messages.where("id > :offset_id", offset_id: offset_id) unless offset_id.blank?
    messages = messages.where("destination LIKE :search_destination", search_destination: "%#{search_destination}%") unless search_destination.blank?

    render json: messages.to_json(methods: [:transmission])
  end

  def show
    message = Message.find(params[:id])
    render json: message.to_json(methods: [:transmission])
  end

  def deliver
    request_body = JSON.parse(request.body.read)
    message = Message.new(request_body)

    if message.save
      TransmitMessageJob.perform_later(message)
      success_response(message)
    else
      failure_response(message)
    end
  rescue StandardError => e
    error_text = I18n.t('errors.message_deliver') << ": #{e.message}"
    logger.error error_text
    failure_response(error_text)
  end

  def callback
    request_body = JSON.parse(request.body.read)
    message_context_id = params[:message_context_id]

    transmission = Transmission.new(status: request_body['status'],
                                    message_context_id: message_context_id,
                                    transmission_context_id: request_body['message_id'])

    # Mainly concerned with failed transmission
    if transmission.save
      success_response(transmission)
    else
      failure_response(transmission)
    end
  end
end
