module Sidekiq
  module ApplicationWorker
    include Sidekiq::Worker

    sidekiq_options unique: :while_executing_reschedule
  end
end