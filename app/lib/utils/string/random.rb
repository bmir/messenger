require 'securerandom'

module Utils
  module String
    module Random
      ASCII_MAX_RANGE = 36

      def string_of_length(length)
        SecureRandom.hex[0, length]
      end

      def hashed_random_string
        [8, 8, 16].map { |length| string_of_length(length) }.join('-')
      end
    end
  end
end
