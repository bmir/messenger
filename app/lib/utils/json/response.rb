module Utils
  module Json
    module Response
      include Constants::Http
      include Utils::String::Random
      include Utils::Audits

      def success_response(body)
        json_response(body, true, nil)
      end

      def failure_response(body, failure_code = RESPONSES[:bad_request])
        json_response(body, false, failure_code)
      end

      private

      def json_response(body, success, failure_code)
        context_id = hashed_random_string
        status_code = success ? :ok : failure_code
        response = { response: body, success: success.to_s, context_id: context_id }

        log_invalid_response(body, failure_code, context_id) unless success
        response[:errors] = extract_errors(body) unless success

        render json: response.to_json, status: status_code
      end

      def extract_errors(body)
        body.errors.map(&:full_message).join(', ') if body.is_a? ApplicationRecord
      end
    end
  end
end
