module Utils
  module Audits
    # Used to log application exception scenarios
    def log_error_response(context_id, error_object)
      content = "#{error_object.message}/#{error_object.backtrace}"
      create_error_log(content, context_id)
    end

    # Used to log general invalid request/responses
    def log_invalid_response(body, failure_code, context_id)
      content = "#{body}/#{failure_code}"
      create_error_log(content, context_id)
    end

    private

    def create_error_log(content, context_id)
      error_log = AuditLog.new(context_id: context_id,
                               content: content.to_json)
      error_log.save
    end
  end
end
