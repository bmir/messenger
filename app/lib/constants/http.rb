module Constants
  module Http
    RESPONSES = {
      ok: :ok,
      created: :created,
      bad_request: :bad_request,
      unauthorized: :unauthorized,
      forbidden: :forbidden,
      not_found: :not_found
    }.freeze
  end
end