class TransmitMessageJob < ApplicationJob
  queue_as :default

  def perform(message)
    Messenger::Api::Server::Transmission.deliver_message(message) unless message.blank?
  end
end
