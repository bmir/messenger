require 'sidekiq/web'

Rails.application.routes.draw do
  scope :api do
    resources :messages do
      collection do
        post :deliver
        post :callback
      end
    end
  end

  # Enable to view sidekiq queue information, otherwise
  # can clog logs.
  # mount Sidekiq::Web => '/sidekiq'
end
