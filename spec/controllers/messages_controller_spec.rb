require 'rails_Helper'

RSpec.describe MessagesController, type: :controller do
  describe 'POST /deliver' do
    subject { post :deliver, body: message_request.to_json }

    context 'when valid message request' do
      let(:message_request) do
        {
          destination: '12344567',
          content: 'abc12345'
        }
      end

      it 'returns a success status' do
        expect(response.status).to be(200)
        subject
      end

      it 'creates a new message request' do
        expect { subject }.to change(Message, :count).by(1)
      end
    end

    context 'when invalid message request' do
      let(:message_request) do
        {
          contentment: 'abc12345'
        }
      end

      it 'returns a failure status' do
        subject

        expect(response.status).to_not be(200)
      end

      it 'returns an unsuccessful status' do
        subject

        response_json = response.body.to_json
        expect(response_json['success']).to_not be(true)
      end


      it 'does not create a new message request' do
        expect { subject }.to change(Message, :count).by(0)
      end
    end
  end

  describe 'POST /callback' do
    let(:params) { { message_context_id: '1234-abc-def' } }

    subject { post :callback, body: transmission_response.to_json, params: params }

    context 'when valid transmission response' do
      let(:transmission_response) do
        {
          status: '12344567',
          message_id: 'abc12345'
        }
      end

      it 'returns a success status' do
        expect(response.status).to be(200)
        subject
      end

      it 'creates a new transmission record' do
        expect { subject }.to change(Transmission, :count).by(1)
      end
    end

    context 'when invalid transmission response' do
      let(:transmission_response) do
        {
          contentment: 'abc12345'
        }
      end

      it 'returns a failure status' do
        subject

        expect(response.status).to_not be(200)
      end

      it 'returns an unsuccessful status' do
        subject

        response_json = response.body.to_json
        expect(response_json['success']).to_not be(true)
      end


      it 'does not create a new message request' do
        expect { subject }.to change(Transmission, :count).by(0)
      end
    end
  end
end
