require 'rails_helper'

RSpec.describe Utils::String::Random do
  let(:random_string_class) { Class.new { include Utils::String::Random } }

  describe '#string_of_length' do  
    subject { random_string_class.new.string_of_length(length) }

    context 'when length of 5' do
      let(:length) { 5 }
      it { is_expected.to have_attributes(length: length) }
    end

    context 'when length of 15' do
      let(:length) { 15 }
      it { is_expected.to have_attributes(length: length) }
    end

    context 'when random length' do
      let(:length) { rand(32)+1 }
      it { is_expected.to have_attributes(length: length) }
    end
  end

  describe '#hashed_random_string' do
    subject { random_string_class.new.hashed_random_string }
    let(:length) { 8 + 8 + 16 + 2 }

    it { is_expected.to have_attributes(length: length) }
    it { is_expected.to include('-').twice }

    it 'contains 3 hyphenated character groups' do
      expect(subject.scan(/[a-zA-Z0-9]{8}[-]/).length).to eql(2)
      expect(subject.scan(/[a-zA-Z0-9]{16}/).length).to eql(1)
    end
  end
end
