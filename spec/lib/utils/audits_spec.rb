require 'rails_helper'

RSpec.describe Utils::Audits do
  include Utils::String::Random
  let(:error_log_class) { Class.new { include Utils::Audits } }

  subject { error_log_class.new }

  describe '#log_error_response' do
    let(:error_object) do
      double(:error, message: "Fake error message",
                     backtrace: "Fake trace of code, could be many lines")
    end

    it 'adds new entry based on context_id and error object' do
      context_id = hashed_random_string
      content = "#{error_object.message}/#{error_object.backtrace}"

      expect {
        subject.log_error_response(context_id, error_object)
      }.to change { AuditLog.count }

      expect(AuditLog.last.content).to match(content)
      expect(AuditLog.last.context_id).to eql(context_id)
    end
  end
end
