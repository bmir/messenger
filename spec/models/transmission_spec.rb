require 'rails_helper'

RSpec.describe Transmission, type: :model do
  subject { Transmission.new(params) }

  describe '.new' do
    let(:params) do
      { status: 'invalid', transmission_context_id: 'abc-123-4567', message_context_id: 'abc-12345' }
    end

    context 'when valid parameters' do
      it 'creates a new instance' do
        subject.save

        expect(subject.errors.count).to eql(0)
      end
    end

    context 'when invalid parameters' do
      let(:params) do
        {  }
      end

      it 'creates a new instance' do
        subject.save

        expect(subject.errors.count).to eql(3)
      end
    end
  end
end
