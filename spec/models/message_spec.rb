require 'rails_helper'

RSpec.describe Message, type: :model do
  subject do
    Message.new(content: content,
                 destination: destination)
  end
  describe '.new' do
    let(:content) { 'Msg cont : abc234' }
    let(:destination) { '23329398439' }

    context 'when attributes are valid' do
      it 'saves without errors' do
        subject.save
        expect(subject.errors.count).to eq(0)
        expect(subject.context_id).to_not be_blank
      end
    end

    context 'when attributes are invalid' do
      let(:destination) {}
      let(:content) {}

      it 'saves with errors' do
        subject.save
        expect(subject.errors.count).to eq(2)
        expect(subject.context_id).to be_blank
      end
    end
  end
end
