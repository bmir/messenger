require 'rails_helper'

RSpec.describe AuditLog, type: :model do
  subject do
    AuditLog.new(content: content,
                 context_id: context_id)
  end
  describe '.new' do
    let(:content) { 'abc234' }
    let(:context_id) { 'asd-sdf-sdfsf' }

    context 'when attributes are valid' do
      it 'saves without errors' do
        subject.save
        expect(subject.errors.count).to eq(0)
      end
    end

    context 'when attributes are invalid' do
      let(:context_id) {}
      let(:content) {}

      it 'saves with errors' do
        subject.save
        expect(subject.errors.count).to eq(2)
      end
    end
  end
end
