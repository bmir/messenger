require 'rails_helper'

RSpec.describe TransmitMessageJob, type: :job do
  subject { TransmitMessageJob.new }

  describe '#perform' do
    context 'when message is valid' do
      let(:message) { double(:message) }

      it 'performs message delivery' do
        expect(Messenger::Api::Server::Transmission).to receive(:deliver_message).with(anything)
        subject.perform(message)
      end
    end

    context 'when message is invalid' do
      let(:message) {}

      it 'does not perform message delivery' do
        expect(Messenger::Api::Server::Transmission).to_not receive(:deliver_message).with(anything)
        subject.perform(message)
      end
    end
  end
end
